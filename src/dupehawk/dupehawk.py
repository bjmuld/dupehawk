#!/usr/bin/env python3

import argparse
import fnmatch
import hashlib
import networkx as nx
import os
import re
import subprocess
import sys
import threading
import time

import progressbar

import numpy as np
from numpy import argsort, array, Inf, exp, clip, ceil
from numpy.random import choice, default_rng
from pathlib import Path

DEFAULT_BLOCKSIZE = 65535
DEFAULT_SAMPLE_RATE = 1
FASTHASH_MAX_BYTES = 50 * 2**20 # 50MB
FASTHASH_MIN_BYTES = 5 * 2 **10 # 5kB
GENERATOR = default_rng()
haltflag = threading.Event()


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    LTBLUE = '\033[96m'
    LTGRAY = '\033[37m'
    DKGRAY = '\033[90m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class Inode(object):
    def __init__(self, node_no, files):
        self.inode = node_no
        if not isinstance(files, list):
            files = [files, ]
        self.files = sorted(files)

    def insert_files(self, item):
        if isinstance(item, str):
            item = [item, ]
        self.files += item
        self.files.sort()

    def trim(self, files):
        if not isinstance(files, list):
            files = [files, ]
        for f in files:
            self.files.remove(f)

    def __ge__(self, other):
        return self.inode >= other.inode

    def __lt__(self, other):
        return self.inode < other.inode

    def __hash__(self):
        return int(self.inode)

    def __repr__(self):
        return str(self.files)

    def __len__(self):
        return len(self.files)

    def __str__(self):
        if len(self.files) == 1:
            return '- {}'.format(self.files[0])
        else:
            return '\n'.join(['* {}'.format(f) for f in self.files])


class SingletonList(list):

    def __init__(self, clist):
        assert isinstance(clist, list)

        clist = sorted(clist, key=lambda item: item[0])

        super().__init__(clist)

    def __str__(self):
        result = []

        for z, f, s, (fi, ino, r) in self:
            result.append(fi)

        return '\n'.join(result)


class DuplicateDict(dict):

    def __init__(self, mapping):

        assert isinstance(mapping, dict)

        super().__init__()

        for k, v in mapping.items():
            sk = sorted(v, key=lambda item: item[0])
            mapping[k] = sk

        keys_ordered = sorted(mapping.keys(), key=lambda item: item[0][0])

        for key in keys_ordered:
            self.update({key: mapping[key]})

    def __str__(self):

        result = []
        c = False

        for s, fl in self.items():
            for fe in fl:
                if args.file_only:
                    result.append('{}'.format(fe[0]))
                else:
                    r = '{}   {}'.format(s[:8], fe[0])
                    if c:
                        r = bcolors.DKGRAY + r + bcolors.ENDC
                    result.append(r)
            c = not c

        return '\n'.join(result)


class CoarseHashTree(dict):

    def __init__(self, path=None):
        global progress_var
        assert isinstance(path, str) or isinstance(path, Path)
        assert Path(path).is_dir()

        tabletree = dict()

        for basedir, subdirs, files in mywalk(path):

            # for each file encountered, add a HT entry
            for f in files:
                fp = Path(basedir) / f

                info = os.stat(fp)
                sz = info.st_size
                ino = info.st_ino

                tabletree[str(fp.relative_to(path))] = (ino, sz)

                progress_var += 1

        super().__init__(tabletree)


class FineHashTree(dict):

    def __init__(self, paths=None, mapping=None):
        global progress_var
        progress_var = 0

        assert not (paths and mapping)

        if paths:

            tabletree = dict()

            for pi, p in enumerate(paths):
                for basedir, subdirs, files in mywalk(p):

                    # for each file encountered, add a HT entry
                    for f in files:

                        item = os.path.join(basedir, f)

                        info = os.stat(item)
                        sz = info.st_size
                        ino = info.st_ino

                        tlet = (item, ino, pi)

                        if sz not in tabletree:
                            tabletree[sz] = tlet

                        else:
                            # collision on size... check fast-hash
                            fhash = fasthash(item)

                            if isinstance(tabletree[sz], tuple):
                                # this is the first collision; existing record is just a file path. replace it with its fasthash
                                tabletree[sz] = {fasthash(tabletree[sz][0]): tabletree[sz]}

                            if fhash not in tabletree[sz]:
                                tabletree[sz][fhash] = tlet

                            else:

                                # collision on fasthash:
                                shash = slowhash(item)

                                if isinstance(tabletree[sz][fhash], tuple):
                                    # this is the first collision; existing record is just a file path. replace it with its slowhash
                                    tabletree[sz][fhash] = {slowhash(tabletree[sz][fhash][0]): [tabletree[sz][fhash]]}

                                if shash not in tabletree[sz][fhash]:
                                    tabletree[sz][fhash][shash] = [tlet]

                                else:
                                    # this is a real collision!
                                    tabletree[sz][fhash][shash].append(tlet)

                        progress_var += 1

            super().__init__(tabletree)

        elif mapping:
            super().__init__(mapping)
            # if not isinstance(mapping, Hashtree):
            #     super(Hashtree, self).__init__(mapping)
            # else:
            #     super(Hashtree, self).__init__()
            #     for h, hte in mapping.items():
            #         self[h] = hte
        else:
            super().__init__()

    def iter_leaves(self):
        for z, ze in self.items():

            if isinstance(ze, tuple):
                yield z, None, None, ze

            else:
                for fh, fe in ze.items():

                    if isinstance(fe, tuple):
                        yield z, fh, None, fe

                    else:
                        for sh, se in fe.items():
                            assert isinstance(se, list)
                            for lel in se:
                                yield z, fh, sh, lel

    def duplicates(self):

        dupes = {}

        for z, f, s, t in self.iter_leaves():
            if s and len(self[z][f][s]) > 1:
                if s not in dupes:
                    dupes[s] = self[z][f][s]
                else:
                    assert t in dupes[s]

        return DuplicateDict(dupes)

    def singletons(self):

        singles = []

        for z, f, s, t in self.iter_leaves():
            if s and len(self[z][f][s]) > 1:
                pass
            else:
                singles.append(t)

        return SingletonList(singles)

    def peel(self):

        singles = []
        dupes = {}

        for z, f, s, t in self.iter_leaves():
            if s and len(self[z][f][s]) > 1:
                if s in dupes:
                    dupes[s] += [t]
                else:
                    dupes[s] = [t]
            else:
                singles.append(t)

        return SingletonList(singles), DuplicateDict(dupes)


def slowhash(filepath):
    # global slowhashcache

    # ino = os.stat(filepath).st_ino
    # if ino in slowhashcache:
    #     return slowhashcache[ino]
    # else:
    # hasher = hashlib.md5()
    # with open(filepath, 'rb') as afile:
    #     buf = afile.read(def_blocksize)
    #     while len(buf) > 0:
    #         hasher.update(buf)
    #         buf = afile.read(blocksize)
    # h = str(hasher.hexdigest())
    return subprocess.check_output(['md5sum', filepath])[:32].decode()
    # if len(slowhashcache) >= 65535:
    #     slowhashcache.popitem(last=False)
    # slowhashcache[ino] = h
    # return h


def fasthash(filepath):
    fz = os.stat(filepath).st_size
    sample_points = (fz/FASTHASH_NUM_SAMPLES/2 + np.arange(FASTHASH_NUM_SAMPLES) * fz/FASTHASH_NUM_SAMPLES).astype(int)
    ssz = FASTHASH_SAMPLE_RATE * fz
    ssz = int(clip(ssz, FASTHASH_MIN_BYTES, FASTHASH_MAX_BYTES))
    per_s_sz = int(ssz/FASTHASH_NUM_SAMPLES)

    h = hashlib.md5()
    with open(filepath, 'rb') as f:
        for ind in sample_points:
            f.seek(ind, 0)
            h.update(f.read(per_s_sz))

    return h.hexdigest()


def mywalk(path):
    for r, dd, ff in os.walk(path, topdown=True, followlinks=False):

        # include and exclude on subdirs
        if args.include or args.exclude:
            dd = [d for d in dd if not skippath(os.path.join(r, d))]
            ff = [f for f in ff if not skippath(os.path.join(r, f))]

        # ff = [f for f in ff if os.path.isfile(os.path.join(r,f))]
        yield r, dd, ff


def count_files(path):
    ct = 0
    for r, dd, ff in mywalk(path):
        ct += len(ff)
    return ct


def sanity_hash_sample():
    global progress_var, status_var, status_message, progress_total
    progress_total = len(path_tree_intersection)
    progress_var = 0
    differing_files = []
    status_var = 0
    status_message = "%(status_var)i inconsistencies"

    chosen_inds = list(range(len(path_tree_intersection)))
    GENERATOR.shuffle(chosen_inds)

    list_view = list(path_tree_intersection.keys())

    for idx in chosen_inds:
        hash = []
        for rd in rootdirs:
            hash += [fasthash(rd / list_view[idx])]
        if not all([h == hash[0] for h in hash]):
            differing_files += [list_view[idx]]
            status_var = len(differing_files)
        progress_var += 1

    print("sampled {:2.1f} percent of all files **(subject to max and min)**\n"
          "and found {} differing files:.".format(FASTHASH_SAMPLE_RATE * 100, len(differing_files)))
    print('    '+'\n    '.join(differing_files))
    status_message = None
    status_var = None
    progress_total = None
    progress_var = None


def print_progress(interval):
    start_time = time.time()
    time.sleep(interval)
    record = {
        'elapsed_time': [],
        'progress': [],
        'prediction': [],
    }
    if status_message is not None and status_var is not None:
        widgets = [
            progressbar.Percentage(),
            progressbar.Bar(marker='\x1b[32m#\x1b[39m'), ' ',
            progressbar.AdaptiveETA(), ' | ',
            progressbar.FormatCustomText(status_message, {'status_var': status_var})
        ]
    else:
        widgets = [
            progressbar.Percentage(),
            progressbar.Bar(marker='\x1b[32m#\x1b[39m'), ' ',
            progressbar.AdaptiveETA(), ' | ',
        ]

    with progressbar.ProgressBar(max_value=progress_total, widgets=widgets, redirect_stdout=True) as bar:
        while not haltflag.is_set():
            bar.update(progress_var)
            haltflag.wait(interval)

    if args.debug:
        record['total_time'] = time.time() - start_time
        from pandas import DataFrame as DF
        cands = list(filter(lambda x: fnmatch.fnmatch(x, 'progress_record_*.dupehawk.csv'), os.listdir()))
        if not cands:
            DF(record).to_csv('progress_record_0.dupehawk.csv')
        else:
            index = sorted([int(c.split('.')[0].split('_')[-1]) for c in cands])[-1] + 1
            DF(record).to_csv('progress_record_{}.dupehawk.csv'.format(index))


def with_status_monitor(function_handle, *args, **kwargs):
    # create thread to print status:
    status = threading.Thread(target=print_progress, args=(2,), daemon=True)
    # start watcher thread
    status.start()
    try:
        result = function_handle(*args, **kwargs)
    finally:
        haltflag.set()
        status.join()
        haltflag.clear()
    return result


# def merge_tables():
#     global hashtree
#
#     lot = list(hashtree.values())
#
#     ntab = lot[0]
#     for otab in lot[1:]:
#         for hte in otab.values():
#             ntab.insert(hte)
#         del otab
#         gc.collect()
#
#     hashtree = ntab
#     gc.collect()


# def format_entry(hash, size, file, link=False):
#     if not args.file_only:
#         if not link:
#             return '{} {:>8s} - \"{}\"'.format(hash[:12], format_bytes(size), file)
#         else:
#             return '{} {:>8s} * \"{}\"'.format(hash[:12], format_bytes(size), file)
#     else:
#         return '\"{:s}\"'.format(file)


def skippath(item):
    # # skip if item not in includes
    # if args.include is not None:
    #     abort = True
    #     for pat in args.include:
    #         if ismatch(f, pat):
    #             abort = False
    #             break
    #     if abort:
    #         continue
    if args.include and not any([ismatch(item, iitem) for iitem in args.include]):
        return True

    # # skip if item in excludes
    # if args.exclude is not None:
    #     abort = False
    #     for pat in args.exclude:
    #         if ismatch(f, pat):
    #             abort = True
    #             break
    #     if abort:
    #         continue
    if args.exclude and any([ismatch(item, eitem) for eitem in args.exclude]):
        return True

    # skip symlinks
    if os.path.islink(item) or not os.path.exists(item):
        return True

    return False


def edie(message, code=os.EX_SOFTWARE):
    print(message, file=sys.stderr)
    sys.exit(code)


def format_bytes(size):
    # 2**10 = 1024
    power = 2 ** 10
    n = 0
    power_labels = {0: '', 1: 'k', 2: 'M', 3: 'G', 4: 'T'}
    while size > power:
        size /= power
        n += 1
    return '{:5.1f}'.format(size) + power_labels[n] + 'B'


def confirm(prompt=None, default_response=False):
    """prompts for yes or no response from the user. Returns True for yes and
    False for no.

    'resp' should be set to the default value assumed by the caller when
    user simply types ENTER.

    >>> confirm(prompt='Create Directory?', default_response=True)
    Create Directory? [y]|n:
    True
    >>> confirm(prompt='Create Directory?', default_response=False)
    Create Directory? [n]|y:
    False
    >>> confirm(prompt='Create Directory?', default_response=False)
    Create Directory? [n]|y: y
    True

    """

    if prompt is None:
        prompt = 'Confirm?'

    if default_response:
        prompt = '%s [%s]|%s|%s: ' % (prompt, 'y', 'n', 'Maybe')
    else:
        prompt = '%s [%s]|%s|%s: ' % (prompt, 'n', 'y', 'Maybe')

    try:
        while True:
            if sys.version_info.major > 2:
                ans = input(prompt)
            else:
                ans = raw_input(prompt)

            if not ans:
                return default_response

            if ans not in ['y', 'Y', 'n', 'N', 'm', 'M']:
                print
                'please enter y or n or m.'
                continue

            if ans.lower() == 'y':
                return True

            if ans.lower() == 'n':
                return False

            if ans.lower() == 'm':
                return None

            time.sleep(1e-3)

    except (KeyboardInterrupt, BdbQuit) as e:
        raise SystemExit

    print('')


def build_coarse_trees(paths):
    global progress_var, progress_total
    progress_var = 0
    progress_total = sum(file_counts)
    result = {}
    for p in paths:
        result[p] = CoarseHashTree(p)
    return result


def parse_cl():
    parser = argparse.ArgumentParser()

    # General Options
    gen_opts = parser.add_argument_group(title='General',
                                         description="options that control the general behavior of Dupehawk")
    gen_opts.add_argument('-d', '--diff-mode', action='store_true',
                          help='compare contents of paths seperately; do not look for duplicates inside individual paths')
    gen_opts.add_argument('-D', '--same-directory', action='store_true',
                          help='only look for duplicates existing in the same directory.')
    gen_opts.add_argument('-B', '--block-size', default=str(DEFAULT_BLOCKSIZE), action='store',
                          help='sets the size of sampled blocks')
    gen_opts.add_argument('-S', '--speed', type=int, default=0,
                          help='take shortcuts to speed-up check. higher integers increase speed but decrease certainty of consistency. default is 0')
    gen_opts.add_argument('--delete', action='append',
                          help='Delete all duplicates found in this path.')
    gen_opts.add_argument('-s', '--sanity-hash-sample', action='store_true')
    gen_opts.add_argument('--debug', action='store_true')
    gen_opts.add_argument('paths', nargs='+',
                          help='paths to scan for duplicates')

    # Matching Options
    match_opts = parser.add_argument_group(title='matching',
                                           description="options that control inclusion and exclusion pattern matching")
    match_opts.add_argument('-i', '--include', action='append',
                            help='pattern for file inclusion. repeat option for multiple inclusions')
    match_opts.add_argument('-x', '--exclude', action='append',
                            help='pattern for file exclusion. repeat option for multiple exclusions')
    match_opts.add_argument('-e', '--regex', action='store_true', help='')

    # Output control
    output_opts = parser.add_argument_group(title='output',
                                            description="options that control output formatting")
    output_opts.add_argument('-f', '--file-only', action='store_true', help='print filepaths only')
    output_opts.add_argument('-r', '--reference-path', action='append',
                             help='\'preferred\' paths. files on path will be suppressed in output')
    output_opts.add_argument('-t', '--target-path', action='append',
                             help='\'target\' paths. files on path will be included in output')
    output_opts.add_argument('-v', '--invert', action='store_true', help='print non-duplicates only')

    args = parser.parse_intermixed_args()

    if args.include is not None:
        args.include = [item.strip('\'\"') for item in args.include]
    if args.exclude is not None:
        args.exclude = [item.strip('\'\"') for item in args.exclude]

    if args.delete and not (args.reference_path or args.target_path or args.same_dir):
        edie('must use -t|-r|-s with \'--delete\' so dupehawk knows which copy to keep')

    if args.diff_mode and len(args.paths) == 1:
        args.path.append(os.getcwd())

    # blocksize = int(args.blocksize)
    # alg_req = args.algorithm.lower()
    #
    # if alg_req == 'md5':
    #     def filehash_fn(filepath):
    #         hasher = hashlib.md5()
    #         with open(filepath, 'rb') as afile:
    #             buf = afile.read(blocksize)
    #             while len(buf) > 0:
    #                 hasher.update(buf)
    #                 buf = afile.read(blocksize)
    #         return str(hasher.hexdigest())
    #
    # if alg_req == 'imo':
    #     def filehash_fn(filepath):
    #         return hashfile(filepath, hexdigest=True)
    #
    # filehash = filehash_fn

    return args


def init():
    global args, ismatch, FASTHASH_SAMPLE_RATE, FASTHASH_NUM_SAMPLES, status_message, status_var, progress_total, progress_var

    status_message = None
    status_var = None
    progress_total = None
    progress_var = None

    args = parse_cl()

    # define string matching function
    if args.regex:
        ismatch = lambda string, pattern: re.match(pattern, string) is not None
    else:
        ismatch = lambda string, pattern: fnmatch.fnmatch(string, pattern)

    FASTHASH_SAMPLE_RATE = clip(1 /2**args.speed, 0, 10)
    FASTHASH_NUM_SAMPLES = 5


def main():
    global file_counts, fine_hashtree, path_trees, rootdirs, path_tree_intersection, path_tree_difference

    init()

    # define rootdirs
    # rootdirs = [os.path.realpath(os.path.expanduser(os.path.expandvars(p))) for p in args.path]
    rootdirs = [Path(os.path.expandvars(p)).expanduser() for p in args.paths]

    if args.target_path:
        rootdirs += [Path(os.path.expandvars(p)).expanduser() for p in args.target_path]
    if args.reference_path:
        rootdirs += [Path(os.path.expandvars(p)).expanduser() for p in args.reference_path]

    test = [item.is_dir() for item in rootdirs]
    if not all(test):
        for i, item in enumerate(rootdirs):
            if not test[i]:
                edie('Cannot find directory \"{}\".'.format(item))

    # do initial scan to get counts:
    file_counts = [count_files(rd) for rd in rootdirs]

    print('building path tree')
    path_trees = with_status_monitor(build_coarse_trees, rootdirs)

    print('finding common filenames')
    path_tree_intersection = dict()
    for i, (root, tab) in enumerate(path_trees.items()):
        if not i:
            path_tree_intersection = tab
            continue
        path_tree_intersection = {k: path_tree_intersection[k] for k in path_tree_intersection.keys() & tab.keys()}
    print('{}/{} ({:2.1f}%) of filenames in common'.format(len(path_tree_intersection) * 2, sum(file_counts),
                                                           len(path_tree_intersection) * 2 / sum(file_counts)*100))

    print('finding unique filenames')
    path_tree_difference = {}
    for i, (root, tab) in enumerate(path_trees.items()):
        path_tree_difference[root] = {k: tab[k] for k in tab.keys() - path_tree_intersection.keys()}
    uq_vec = [len(a.keys()) for a in path_tree_difference.values()]
    print('{}/{} ({:2.1f}%) of filenames are unique.'.format(sum(uq_vec), sum(file_counts), sum(uq_vec) / sum(file_counts)*100))
    print('Unique filenames in each path: {}'.format(uq_vec))
    for rp, dt in path_tree_difference.items():
        if dt:
            print('{} exclusive:'.format(rp))
            print('    '+ '\n    '.join(dt))

    if args.sanity_hash_sample:
        print('Sampling data within common files')
        with_status_monitor(sanity_hash_sample)
        return 0

    # build hashtable
    print('building hashtable')
    fine_hashtree = with_status_monitor(FineHashTree, kwargs={'paths':rootdirs})

    if args.diff_mode:
        process_tree_diffs(rootdirs)

    else:
        process_tree()


def process_tree():
    dupes = fine_hashtree.duplicates()

    if not (args.target_path or args.reference_path or args.same_dir):
        # keep all
        kf = dupes

    else:
        # empty dict for addition:
        kf = DuplicateDict()

        if args.target_path:
            for dh, dl in dupes.items():

                tar_mch = [any([ismatch(iitem, pstr)
                                for pstr in args.target_path
                                for iitem in os.path.dirname(de[0]).split(os.sep)])
                           for de in dl]

                if not any(tar_mch) or all(tar_mch):
                    continue
                else:
                    if dh not in kf:
                        kf[dh] = [de for di, de in enumerate(dl) if tar_mch[di]]
                    else:
                        kf[dh].extend([de for di, de in enumerate(dl) if tar_mch[di]])

        if args.reference_path:
            for dh, dl in dupes.items():

                ref_mch = [any([ismatch(iitem, pstr)
                                for pstr in args.reference_path
                                for iitem in os.path.dirname(de[0]).split(os.sep)])
                           for de in dl]

                if not any(ref_mch) or all(ref_mch):
                    continue
                else:
                    if dh not in kf:
                        kf[dh] = [de for di, de in enumerate(dl) if not ref_mch[di]]
                    else:
                        kf[dh].extend([de for di, de in enumerate(dl) if not ref_mch[di]])

        if args.same_dir:

            for dh, dl in dupes.items():
                for i, di in enumerate(dl[:-1]):
                    for j, dj in enumerate(dl[i + 1:]):
                        if os.path.dirname(di[0]) == os.path.dirname(dj[0]):
                            if dh in kf:
                                kf[dh].append(di[0] if dj[0] > di[0] else dj[0])
                            else:
                                kf[dh] = [di[0] if dj[0] > di[0] else dj[0]]

    if args.delete:
        if len(kf) > 0:

            assert args.target_path or args.reference_path or args.same_dir
            response = None
            while response is None:
                print(kf)
                response = confirm('about to delete {} files. Are you sure?'.format(len(kf)),
                                   default_response=False)
            if not response:
                raise SystemExit(0)
            else:
                for f in kf:
                    os.remove(f[0])
        return

    else:
        print(kf)


def process_tree_diffs(rootdirs):
    singletons = fine_hashtree.singletons()
    if singletons:
        sd = {}
        for se in singletons:
            ri = se[-1]
            if ri not in sd:
                sd[ri] = [se]
            else:
                sd[ri].append(se)

        for ri, sl in sd.items():
            print('')
            print('#' * 80)
            print('#' * 80)
            print('## Items found only in {}:'.format(rootdirs[ri]))
            print('#' * 80)
            print('#' * 80)
            for se in sl:
                print(se[0])

    duplicates = fine_hashtree.duplicates()
    if duplicates:
        dds = {}
        for dh, dl in duplicates.items():
            ms = tuple(sorted(list(set([de[-1] for de in dl]))))
            if ms not in dds:
                dds[ms] = DuplicateDict({dh: dl})
            else:
                dds[ms].update({dh: dl})

        for dn, dd in dds.items():
            print('\n')
            print('#' * 80)
            print('## Items common to:')
            for ri in dn:
                print('##     {}'.format(rootdirs[ri]))
            print('')

            print(dd)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        raise SystemExit(0)
    finally:
        print('')
